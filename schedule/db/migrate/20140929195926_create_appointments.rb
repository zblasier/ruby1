class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :specialist_id
      t.integer :patient_id
      t.string :complaint
      t.string :text
      t.string :appointment_date
      t.string :date
      t.string :appointment_fee
      t.string :decimal

      t.timestamps
    end
  end
end
